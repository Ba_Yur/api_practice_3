import 'package:flutter/material.dart';

import 'models/product.dart';

class ProductDetail extends StatelessWidget {
  final Product product;

  const ProductDetail({this.product});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(product.name),
      ),
      body: Hero(
        tag: product.id,
        child: Material(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Center(
                    child: Container(
                      height: 300,
                      child: product.imageURL != null
                          ? Image.network(
                              product.imageURL,
                              fit: BoxFit.cover,
                            )
                          : 'https://clients.cylindo.com/viewer/3.x/v3.0/documentation/img/not_found.gif',
                    ),
                  ),
                  Text(
                    product.brand,
                    style: TextStyle(
                      fontSize: 50,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(product.description),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Text('Buy for: ${product.price} \$'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
