class Product {
  final int id;
  final String name;
  final String brand;
  final String price;
  final String imageURL;
  final String description;

  Product(
      {this.id,
      this.name,
      this.brand,
      this.price,
      this.imageURL,
      this.description});

}
