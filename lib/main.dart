import 'package:api_practice_3/models/product.dart';
import 'package:api_practice_3/product_detail.dart';
import 'package:api_practice_3/repository.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String param = 'maybelline';
  List<Product> productList = [];

  void getData() async {
    productList = [];
    final data = await http.get(
        'http://makeup-api.herokuapp.com/api/v1/products.json?brand=$param');
    List<dynamic> listData = json.decode(data.body);
    for (Map prod in listData) {
      print(prod['brand']);
      setState(
        () {
          productList.add(
            Product(
              id: prod['id'],
              name: prod['name'],
              brand: prod['brand'],
              price: prod['price'],
              imageURL: prod['image_link'],
              description: prod['description'],
            ),
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('API practice 3'),
      ),
      body: Center(
        child: Column(
          children: [
            TextField(
              onSubmitted: (value) {
                print(value);
                setState(() {
                  param = value;
                });
                print(param);
              },
            ),
            ElevatedButton(
                child: Text('Tap to search'),
                onPressed: () {
                  getData();
                }),
            productList.length == 0
                ? SizedBox()
                : SingleChildScrollView(
                  child: Container(
                      height: 550,
                      child: GridView.count(
                        mainAxisSpacing: 10.0,
                        crossAxisCount: 2,
                        children: [
                          ...productList.map(
                            (product) {
                              return Hero(
                                tag: product.id,
                                child: Material(
                                  child: Container(
                                    height: 150,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ProductDetail(
                                              product: product,
                                            ),
                                          ),
                                        );
                                      },
                                      child: ClipRRect(
                                        child: Container(
                                          margin: EdgeInsets.all(10.0),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: NetworkImage(product.imageURL !=
                                                        null
                                                    ? product.imageURL
                                                    : 'https://clients.cylindo.com/viewer/3.x/v3.0/documentation/img/not_found.gif'),
                                                fit: BoxFit.cover),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                width: double.infinity,
                                                height: 30,
                                                color: Colors.black54,
                                                child: Text(
                                                  product.name,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.white),
                                                ),
                                              ),
                                              Container(
                                                width: double.infinity,
                                                height: 30,
                                                color: Colors.black54,
                                                child: Center(
                                                  child: Text(
                                                    '${product.price} \$',
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 20,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                ),
          ],
        ),
      ),
    );
  }
}
